# Guía 4 - Unidad II

Los archivos contenidos en este repositorio consisten en la creación de un árbol binario de búsqueda especificados en el archivo pdf del mismo.

### Descripción
Este algoritmo consiste en la creación de un árbol binario de búsqueda que contiene solo números enteros. Se le preguntará al usuario un número inicial para crear el árbol, y a continuación se mostrará un menú con algunas opciones. Mediante este menú, el usuario podrá agregar números, eliminar números, modificar números, mostrar el contenido de árbol en preorden, inorden y posorden, generar un grafo del árbol el cuál se puede visualizar como archivo .png, y salir del programa.

### Ejecución
El algoritmo cuenta con su makefile, por lo que con el comando "make" se generará un ejecutable llamado "programa", el cuál se debe ejecutar con el comando "./programa", lo que permitirá el usuario iniciar el algoritmo.

### Construido con
El presente proyecto de programación, se construyó en base al lenguaje de programación C++, en conjunto con el IDE "Visual Studio Code".

* [C++] - Lenguaje de programación utilizado.
* [Visual Studio Code](https://code.visualstudio.com/) - IDE utilizado.

### Autores
* **Daniel Tobar** - Estudiante de Ingeniería Civil en Bioinformática.
