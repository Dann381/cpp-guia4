#include <fstream>
#include <string.h>
#include <iostream>
using namespace std;

typedef struct _Nodo {
    int info;
    struct _Nodo *izq;
    struct _Nodo *der;
} Nodo;

/* 
 * crea un nuevo nodo.
 */
Nodo *crearNodo(int dato) {
    Nodo *q;

    q = new Nodo();
    q->izq = NULL;
    q->der = NULL;
    q->info = dato;
    return q;
}

void reemplazar(Nodo** act){
    Nodo* a, *p;
    p = *act;

    a = (*act)->izq;
    while(a->der){
        p = a;
        a = a->der;
    }
    (*act)->info = a->info;
    if (p == (*act)){
        p->izq = a->izq;
    }
    else{
        p->der = a->izq;
    }
    (*act) = a;
}

void preorden(Nodo *raiz){
    if (raiz != NULL){
        cout << raiz->info << " - ";

        preorden(raiz->izq);
        preorden(raiz->der);
    }
}

void inorden(Nodo *raiz){
    if (raiz != NULL){
        inorden(raiz->izq);
        cout << raiz->info << " - ";
        inorden(raiz->der);
    }

}

void posorden(Nodo *raiz){
    if (raiz != NULL){
        posorden(raiz->izq);
        posorden(raiz->der);
        cout << raiz->info << " - ";
    }
}

void eliminar(Nodo **raiz, int dato){
    if ((*raiz) != NULL){
        if (dato < (*raiz)->info){
            eliminar(&(*raiz)->izq, dato);
        }
        else if (dato > (*raiz)->info){
            eliminar(&(*raiz)->der, dato);
        }
        else if (dato == (*raiz)->info){
            Nodo* otro;
            otro = (*raiz);
            if (otro->der == NULL && otro->izq != NULL){
                (*raiz) = otro->izq;
            }
            else if(otro->izq == NULL && otro->der != NULL){
                (*raiz) = otro->der;
            }
            else if(otro->izq != NULL && otro->der != NULL) {
                reemplazar(&otro);
            }
            else{
                *raiz = NULL;
            }
            free(otro);
        }
    }
    else{
        cout << "El número a eliminar no se encuentra en el árbol" << endl;
    }
}

void agregar(Nodo *raiz, int dato){
    if (dato < raiz->info && raiz->izq == NULL){
        raiz->izq = crearNodo(dato);
    }
    else if (dato > raiz->info && raiz->der == NULL){
        raiz->der = crearNodo(dato);
    }
    else if (dato < raiz->info && raiz->izq != NULL){
        agregar(raiz->izq, dato);
    }
    else if (dato > raiz->info && raiz->der != NULL){
        agregar(raiz->der, dato);
    }
    else if (dato = raiz->info){
        cout << "El dato ya se encuentra agregado" << endl;
    }
}

/*
 * crea el archivo .txt de salida y le agrega
 * contenido del árbol para generar el grafo (imagen).
 */

class Grafo {
    private:
    
    public:
        Grafo (Nodo *nodo) {
            ofstream fp;
            
            /* abre archivo */
            fp.open ("grafo.txt");

            fp << "digraph G {" << endl;
            fp << "node [style=filled fillcolor=yellow];" << endl;
                
            recorrer(nodo, fp);

            fp << "}" << endl;

            /* cierra archivo */
            fp.close();
                    
            /* genera el grafo */
            system("dot -Tpng -ografo.png grafo.txt &");
            
            /* visualiza el grafo */
            system("eog grafo.png &");
        }
        
        /*
        * recorre en árbol en preorden y agrega datos al archivo.
        */
        void recorrer(Nodo *p, ofstream &fp) {
            string cadena = "\0";
              
            if (p != NULL) {
                if (p->izq != NULL) {
                    fp <<  p->info << "->" << p->izq->info << ";" << endl;
                    cout << "\n";
                } else {      
                    cadena = to_string(p->info) + "i";
                    cadena = '"' + cadena + '"';
                    fp <<  cadena << " [shape=point]" << endl;
                    fp << p->info << "->" << cadena << ";" << endl;
                }
                cadena = p->info;
                if (p->der != NULL) { 
                    fp << p->info << "->" << p->der->info << ";" << endl;
                } else {
                    cadena = to_string(p->info) + "d";
                    cadena = '"' + cadena + '"';
                    fp <<  cadena << " [shape=point]" << endl;
                    fp << p->info << "->" << cadena << ";" << endl;
                }

                recorrer(p->izq, fp);
                recorrer(p->der, fp); 
            }
        }
};

/*
 * función principal.
 */
int main(void) {
    Nodo *raiz = NULL;

    int numero;
    cout << "Ingrese un número para iniciar el árbol" << endl;
    cout << "Número: ";
    cin >> numero;
    raiz = crearNodo(numero);

    agregar(raiz, 87);
    agregar(raiz, 140);
    agregar(raiz, 43);
    agregar(raiz, 99);
    agregar(raiz, 130);
    agregar(raiz, 22);
    agregar(raiz, 65);
    agregar(raiz, 93);
    agregar(raiz, 135);
    agregar(raiz, 56); 


    int opt = 0;
    while (opt != 6){
        cout << "-------------- Menú --------------" << endl;
        cout << "[1] Agregar número" << endl;
        cout << "[2] Eliminar número" << endl;
        cout << "[3] Modificar número" << endl;
        cout << "[4] Mostrar contenido del árbol" << endl;
        cout << "[5] Generar un grafo del árbol" << endl;
        cout << "[6] Salir" << endl;
        cout << "Opción: ";
        cin >> opt;
        if(opt == 1){
            system("clear");
            cout << "Ingrese un número: ";
            cin >> numero;
            agregar(raiz, numero);
        }
        else if(opt == 2){
            system("clear");
            cout << "Ingrese un número: ";
            cin >> numero;
            eliminar((&raiz), numero);
        }
        else if(opt == 3){
            system("clear");
            cout << "Ingrese un número para modificar: ";
            cin >> numero;
            eliminar((&raiz), numero);
            cout << "Ingrese un nuevo número para agregar: ";
            cin >> numero;
            agregar(raiz, numero);
        }
        else if(opt == 4){
            system("clear");
            cout << "Mostrando contenido del árbol" << endl;
            cout << "Preorden: ";
            preorden(raiz);
            cout << endl;
            cout << "Inorden: ";
            inorden(raiz);
            cout << endl;
            cout << "Posorden: "; 
            posorden(raiz);
            cout << endl;
        }
        else if(opt == 5){
            system("clear");
            Grafo *g = new Grafo(raiz);
            cout << "Grafo del árbol generado" << endl;
        }
        else if(opt == 6){
            system("clear");
            cout << "Saliendo..." << endl;
        }
        else{
            system("clear");
            cout << "Ingrese un número válido" << endl;
            break;
        }
    }
    
    return 0;
}
